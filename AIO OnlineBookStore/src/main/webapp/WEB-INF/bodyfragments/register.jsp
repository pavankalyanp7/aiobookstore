<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Sign Up</title>
</head>
<body>
	<jsp:include page="header.jsp"/>
	<table id="table">
		<tr>
			<td><img alt="login" src="images/login.png"></td>
			<td>
				<form action="/signUp" method="post">
					<fieldset><legend>Register</legend>
						<table>
							<tr>
								<td>Mobile Number:</td>
								<td><input type="text" name="usermobilenumber" value="${usermobilenumber}"></td>
							</tr>
							<tr>
								<td>First Name:</td>
								<td><input type="text" name="userfirstname"></td>
							</tr>
							<tr>
								<td>Last Name:</td>
								<td><input type="text" name="userlastname"></td>
							</tr>
							<tr>
								<td>Pin code:</td>
								<td><input type="number" name="citypincode" pattern="[1-9]{1}[0-9]{5}"></td>
							</tr>
							<tr>
								<td>Email:</td>
								<td><input type="email" name="useremail"></td>
							</tr>
							<tr>
								<td>Gender:</td>
								<td><input type="radio" name="usergender" value="male">Male <input type="radio" name="usergender" value="female">Female</td>
							</tr>
							<tr>
								<td>Date of Birth:</td>
								<td><input type="date" name="userdob"></td>
							</tr>
							<tr>
								<td>Address:</td>
								<td><input name="useraddress"></td>
							</tr>
							<tr>
								<td colspan="2"><button type="submit">Submit</button></td>
							</tr>
						</table>
					</fieldset>
				</form>
			</td>
		</tr>
	</table>
</body>
</html>