package in.co.obs.ctl;


public interface DropdownList
{
	public String getKey();

	public String getValue();
}
