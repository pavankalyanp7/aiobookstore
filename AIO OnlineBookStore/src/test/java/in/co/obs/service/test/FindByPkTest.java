package in.co.obs.service.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.autoconfigure.security.SecurityProperties.User;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import in.co.obs.dto.UserDTO;
import in.co.obs.exception.DuplicateRecordException;
import in.co.obs.service.UserServiceImpl;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
public class FindByPkTest {

	@MockBean
	private UserDTO dto;
	
	@InjectMocks
	private UserServiceImpl service;
	
	@Test
	public void testFindByPk() 
	{
		Integer id =  null;
		String pk = "Pavan";
		Mockito.when(dto.getFirstName(pk)).thenReturn(null);
		boolean result = service.equals(pk);
		assertFalse(pk);
	}
	private void assertFalse(String pk) {
	}

	@Test
	public void testFindByPk2() 
	{
		Integer id = null;
		String pk = "Kalyan";
		Mockito.when(dto.getFirstName(pk)).thenReturn(pk);
		boolean result = service.equals(pk);
		assertFalse(pk);
	}
	
	@Test
	public void testFindByPk3() 
	{
		Integer id = null;
		String pk = "Mamatha";
		Mockito.when(dto.getFirstName(pk)).thenReturn(pk);
		boolean result = service.equals(pk);
		assertFalse(pk);
	}
	
	@Test
	public void testFindByPk4() 
	{
		Integer id = null;
		String pk = "Mahesh";
		Mockito.when(dto.getFirstName(pk)).thenReturn(pk);
		boolean result = service.equals(pk);
		assertFalse(pk);
	}
	
	@Test
	public void testFindByPk5() 
	{
		Integer id = null;
		String pk = "Sudheer";
		Mockito.when(dto.getFirstName(pk)).thenReturn(pk);
		boolean result = service.equals(pk);
		assertFalse(pk);
	}
	
	@Test
	public void testFindByPk6() 
	{
		Integer id = null;
		String pk = "Pradeep";
		Mockito.when(dto.getFirstName(pk)).thenReturn(pk);
		boolean result = service.equals(pk);
		assertFalse(pk);
	}
}
