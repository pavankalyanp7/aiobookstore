package in.co.obs.service.test;

import static org.junit.Assert.assertFalse;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import in.co.obs.dto.BookedDTO;
import in.co.obs.dto.UserDTO;
import in.co.obs.service.UserServiceImpl;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
public class findByBook {

	@MockBean
	private UserDTO dto;
	
	@InjectMocks
	private UserServiceImpl service;
	
	@Test
	public void findbyBooktest() 
	{
		Integer id = null;
		String name = "Wings of fire";
		Mockito.when(dto.getBooks(name)).thenReturn(name);
		boolean result = service.equals(name);
		assertFalse(name);
	}
	
	@Test
	public void findbyBooktest2() 
	{
		Integer id = null;
		String name = "Wings of fire";
		Mockito.when(dto.getBooks(name)).thenReturn(name);
		boolean result = service.equals(name);
		assertFalse(name);
	}
	
	@Test
	public void findbyBooktest3() 
	{
		Integer id = null;
		String name = "Wings of fire";
		Mockito.when(dto.getBooks(name)).thenReturn(name);
		boolean result = service.equals(name);
		assertFalse(name);
	}
	
	@Test
	public void findbyBooktest4() 
	{
		Integer id = null;
		String name = "Wings of fire";
		Mockito.when(dto.getBooks(name)).thenReturn(name);
		boolean result = service.equals(name);
		assertFalse(name);
	}
	
	@Test
	public void findbyBooktest5() 
	{
		Integer id = null;
		String name = "Wings of fire";
		Mockito.when(dto.getBooks(name)).thenReturn(name);
		boolean result = service.equals(name);
		assertFalse(name);
	}
	
	@Test
	public void findbyBooktest6() 
	{
		Integer id = null;
		String name = "Wings of fire";
		Mockito.when(dto.getBooks(name)).thenReturn(name);
		boolean result = service.equals(name);
		assertFalse(name);
	}

	private void assertFalse(String name) {
		// TODO Auto-generated method stub
		
	}

}
